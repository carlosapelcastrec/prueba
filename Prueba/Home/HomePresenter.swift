//
//  HomePresenter.swift
//  Prueba
//
//  Created by Carlos Armando Pelcastre Carmona on 19/11/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import Foundation

class HomePresenter {

    weak var view: HomeView?
    var provider: HomeProvider?
    var wireframe: HomeWireframe?

}

// MARK: - Extensions - Ejecuta en el presenter los llamados del View Controller
extension HomePresenter: HomeEventHandler {
    
}

// MARK: - Extensions - Ejecuta en el presenter los llamados del Interactor
extension HomePresenter: HomeOutput {
    
}
