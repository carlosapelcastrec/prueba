//
//  NameTableViewCell.swift
//  Prueba
//
//  Created by Carlos Armando Pelcastre Carmona on 19/11/21.
//

import UIKit

class NameTableViewCell: UITableViewCell {

    lazy var nameTxtF : UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.placeholder = "Introduzca su nombre"
        textField.keyboardType = UIKeyboardType.alphabet
        textField.returnKeyType = UIReturnKeyType.done
        textField.autocorrectionType = UITextAutocorrectionType.no
        textField.font = UIFont.systemFont(ofSize: 13)
        textField.borderStyle = UITextField.BorderStyle.roundedRect
        textField.clearButtonMode = UITextField.ViewMode.whileEditing;
        textField.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        return textField
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupConstraints(){
        //super.delegate = delegate
        contentView.addSubview(nameTxtF)
        nameTxtF.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 32).isActive = true
        nameTxtF.leftAnchor.constraint(equalTo: contentView.leftAnchor,constant: 20.0).isActive = true
        nameTxtF.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: 20.0).isActive = true
    }
}
