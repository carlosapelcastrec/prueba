//
//  HomeViewController.swift
//  Prueba
//
//  Created by Carlos Armando Pelcastre Carmona on 19/11/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//


import UIKit

final class HomeViewController: UIViewController,UITableViewDataSource,UITableViewDelegate  {


    // MARK: - Public properties -
    var eventHandler: HomePresenter!

    var homeTableView : UITableView!
    var arrayCurrentOrderedContainers : [Any] = []
    var arrayActualContainers : [Any] = []

    // MARK: - Lifecycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        setupTable()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }

    func setupTable(){
        homeTableView = UITableView(frame: .zero)
        homeTableView.estimatedRowHeight = 1000
        homeTableView.rowHeight = UITableView.automaticDimension
        homeTableView.delegate = self
        homeTableView.dataSource = self
        homeTableView.dragInteractionEnabled = true
        homeTableView.backgroundColor = .clear
        homeTableView.separatorStyle = .none
        homeTableView.showsVerticalScrollIndicator = false
        homeTableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(homeTableView)
        homeTableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        homeTableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
        homeTableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        homeTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true

        homeTableView.register(NameTableViewCell.self, forCellReuseIdentifier: "NameTableViewCell")
        homeTableView.register(SelfieTableViewCell.self, forCellReuseIdentifier: "SelfieTableViewCell")
        homeTableView.register(GraphTableViewCell.self, forCellReuseIdentifier: "GraphTableViewCell")
        
    }

    //MARK: Function to sort Containers
    func sortCurrentOrderedContainers(){
        arrayCurrentOrderedContainers.removeAll()
        let arrayOrderPossible = getPossibleOrderedArray()
        for possibleContainer in arrayOrderPossible{
            switch possibleContainer{
            case is NameTableViewCell.Type:
                appendSortedContainer(NameTableViewCell.self)
            case is SelfieTableViewCell.Type:
                appendSortedContainer(SelfieTableViewCell.self)
            case is GraphTableViewCell.Type:
                appendSortedContainer(GraphTableViewCell.self)
            default:
                break
            }
        }
    }
}

// MARK: - Extensions -
extension HomeViewController: HomeView {
    func showName() {
        appendContainerConsumed(consumptionType: NameTableViewCell.self)
        homeTableView.reloadData()
    }
}


extension HomeViewController{

    func appendSortedContainer<T>(_ containerToAdd: T.Type){
        //IF IS POSSIBLE TO ADD
        for container in arrayActualContainers{
            if container as? Any.Type == containerToAdd.self{
                arrayCurrentOrderedContainers.append(containerToAdd)
            }
        }
    }
    
    func appendContainerConsumed<T>(consumptionType: T.Type){
        // IF CONTAINS
        for container in arrayActualContainers{
            if container as? Any.Type == consumptionType.self{
                return
            }
        }
        arrayActualContainers.append(consumptionType)
        sortCurrentOrderedContainers()
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 1000
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch arrayCurrentOrderedContainers[indexPath.row] {
        case is NameTableViewCell.Type:
            var cell = tableView.dequeueReusableCell(withIdentifier: "NameTableViewCell", for: indexPath as IndexPath) as? NameTableViewCell
            //cell?.delegate = self
            return cell!
        default:
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCurrentOrderedContainers.count
    }

    func getPossibleOrderedArray() -> [Any]{
        var array : [Any] = []
        array.append(NameTableViewCell.self)
        array.append(SelfieTableViewCell.self)
        array.append(GraphTableViewCell.self)
        return array
    }
}

extension UIScrollView {

  var safeTopAnchor: NSLayoutYAxisAnchor {
    if #available(iOS 11.0, *) {
      return self.safeAreaLayoutGuide.topAnchor
    } else {
      return self.topAnchor
    }
  }

  var safeLeftAnchor: NSLayoutXAxisAnchor {
    if #available(iOS 11.0, *){
      return self.safeAreaLayoutGuide.leftAnchor
    }else {
      return self.leftAnchor
    }
  }

  var safeRightAnchor: NSLayoutXAxisAnchor {
    if #available(iOS 11.0, *){
      return self.safeAreaLayoutGuide.rightAnchor
    }else {
      return self.rightAnchor
    }
  }

  var safeBottomAnchor: NSLayoutYAxisAnchor {
    if #available(iOS 11.0, *) {
      return self.safeAreaLayoutGuide.bottomAnchor
    } else {
      return self.bottomAnchor
    }
  }
}
