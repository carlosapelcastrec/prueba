//
//  HomeWireframe.swift
//  Prueba
//
//  Created by Carlos Armando Pelcastre Carmona on 19/11/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

class HomeWireframe {
    private var view: HomeViewController?
    private var presenter: HomePresenter?
    private var interactor: HomeInteractor?
    private var window: UIWindow?

    init(in window: UIWindow?) {
        self.view = HomeViewController()
        self.presenter = HomePresenter()
        self.interactor = HomeInteractor()
        
        self.view?.eventHandler = self.presenter
        self.interactor?.output = self.presenter
        self.presenter?.view = self.view
        self.presenter?.provider = self.interactor
        self.presenter?.wireframe = self
        self.window = window
    }

}

