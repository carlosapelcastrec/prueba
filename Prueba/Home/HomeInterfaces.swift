//
//  HomeInterfaces.swift
//  Prueba
//
//  Created by Carlos Armando Pelcastre Carmona on 19/11/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
// MARK: - Protocol - Funcines en las que el Presenter comparte datos con el View Controller
protocol HomeView: class {
    
}
// MARK: - Protocol - Funciones en las que el View Controler al Presenter
protocol HomeEventHandler {
    
}
// MARK: - Protocol - Funciones que comunican al Presenter con el Interactos
protocol HomeProvider {
    
}
// MARK: - Protocol - Funciones en las que el Interactor envia datos al Presenter
protocol HomeOutput: class {
    
}
